<?php


class Router
{

    public static function route(array $url)
    {
        // controller handler - get first param from URL and remove
        $controller = (isset($url[0]) && $url[0] != '') ? ucwords($url[0] . 'Controller') : DEFAULT_CONTROLLER;
        $controllerName = str_replace('Controller', '', $controller);
        array_shift($url);


        // method handler - get first param from (rest of the URL) and remove
        $method = (isset($url[0]) && $url[0] != '') ? ($url[0] . 'Action') : DEFAULT_ACTION;
        $methodName = (isset($url[0]) && $url[0] != '') ? $url[0] : 'index';
        array_shift($url);

        // access control check
        $grantAccess = self::hasAccess($controllerName, $methodName);

        if (!$grantAccess) {
            $controller = ACCESS_RESTRICTED;
            $controllerName = ACCESS_RESTRICTED;
            $method = 'indexAction';
        }

        // parameters - rest of stuff in URL
        $queryParams = $url;

        // call controller and method
        $dispatch = new $controller($controllerName, $methodName);

        // check does method exists
        if (method_exists($controller, $method)) {
            call_user_func_array([$dispatch, $method], $queryParams);
        } else {
            die("Controller " . $controller . "<br>Controller name: " . $controllerName . "<br>method " . $method . " not exists");
        }
    }

    private static function hasAccess($controller, $methodName = 'index')
    {
        // check session and access control
        $acl_file = file_get_contents(ROOT . DS . 'app' . DS . 'acl.json');
        $acl_array = json_decode($acl_file, true);

        $currentUserACLs = ["Guest"];
        $grantAccess = false;

        if (Session::exists(CURRENT_USER_SESSION_NAME)) {
            $currentUserACLs[] = "LoggedIn";
            $acls = currentUser()->acls();
            if (isset($acls)) {
                foreach (currentUser()->acls() as $item) {
                    $currentUserACLs[] = $item;

                }
            }
        }

        foreach ($currentUserACLs as $level) {
            if (array_key_exists($level, $acl_array) && array_key_exists($controller, $acl_array[$level])) {
                // check controller and method
                if (in_array($methodName, $acl_array[$level][$controller]) || in_array("*", $acl_array[$level][$controller])) {
                    $grantAccess = true;
                    break;
                }
            }
        }

        // check denied
        foreach ($currentUserACLs as $accessLevel) {
            $denied = $acl_array[$accessLevel]['denied'];
            if (!empty($denied)
                && array_key_exists($controller, $denied)
                && in_array($methodName, $denied[$controller])) {
                dnd($denied);
                $grantAccess = false;
                break;
            }
        }
        return $grantAccess;
    }

    public function redirect($location)
    {
        if (!headers_sent()) {
            header('Location: ' . PROJECT_ROOT . $location);
            exit();
        } else {
            echo '<script type="text/javascript">';
            echo 'window.location.href="' . PROJECT_ROOT . $location . '";';
            echo '</script>';
            echo '<noscript>';
            echo '<meta http-equiv="refresh" content="0;url=' . $location . '" />';
            echo '</noscript>';
            exit;
        }
    }

    public static function getMenu($menu)
    {
        $menuArray = [];
        $menuFile = file_get_contents(ROOT . DS . 'app' . DS . $menu . '.json');
        $acl = json_decode($menuFile, true);
        foreach ($acl as $key => $value) {
            if (is_array($value)) {
                $submenu = [];
                foreach ($value as $k => $v) {
                    if ($k == 'separator' && !empty($submenu)) {
                        $submenu[$k] = '';
                    } elseif ($finalValue = self::get_link($v)) {
                        $submenu[$k] = $finalValue;
                    } else {
                        $submenu[$k] = $v;
                    }
                }
                if (!empty($submenu)) {
                    $menuArray[$key] = $submenu;
                }
            } else {
                if ($finalValue = self::get_link($value)) {
                    $menuArray[$key] = $finalValue;
                }
            }
        }
        return $menuArray;
    }

    // build links
    public static function get_link($value)
    {
        // check is link external
        if (preg_match('/https?:\/\//', $value) == 1) {
            return $value;
        } else {
            $uArray = explode(DS, $value);
            $controllerName = ucwords($uArray[0]);
            $methodName = (isset($uArray[1])) ? $uArray[1] : '';
            // check persmission
            if (self::hasAccess($controllerName, $methodName)) {
                return PROJECT_ROOT . $value;
            }
            return false;
        }
    }

}