<?php


class FormHelpers
{


    public static function inputBlock($type, $label, $name, $value = '', $inputAttributes = [], $divAttributes)
    {
        $divString = self::tringifyAttributes($divAttributes);
        $inputString = self::stringifyAttributes($inputAttributes);
        $html = '<div ' . $divString . '>';
        $html .= '<label for="' . $name . '">' . $label . '</label>';
        $html .= '<input type="' . $type . '" id="' . $name . '" name="' . $name . '" value="' . $value . '" ' . $inputString . ' />';
        $html .= '</div>';

        return $html;
    }

    public static function submitTag($buttonText, $inputAttributes = [])
    {
        $inputString = self::stringifyAttributes($inputAttributes);

        $html = '<input type="submit" value="' . $buttonText . '" ' . $inputString . ' />';

        return $html;
    }

    public static function submitBlock($buttonText, $inputAttributes, $divAttributes)
    {
        $divString = self::stringifyAttributes($divAttributes);
        $inputString = self::stringifyAttributes($inputAttributes);

        $html = '<div ' . $divString . ' >';
        $html .= '<input type="submit" value="' . $buttonText . '" ' . $inputString . ' />';

        return $html;
    }

    public static function stringifyAttributes($attributes)
    {
        $string = '';

        /**
         * stringify
         * @example class="red" onclick="return false"...
         */
        foreach ($attributes as $key => $value) {
            $string .= ' ' . $key . ' = "' . $value . '"';
        }

        return $string;
    }

    public static function generateToken()
    {
        $token = base64_encode(openssl_random_pseudo_bytes(32));
        Session::set('csrf_token', $token);
        return $token;
    }

    public static function checkToken($token)
    {
        return (Session::exists('csrf_token') && Session::get('csrf_token') == $token);
    }

    public static function csrfInput()
    {
        return '<input type="hidden" name="csrf_token" id="csrf_token" value="' . self::generateToken() . '" />';
    }
}