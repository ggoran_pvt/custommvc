<?php


class Controller extends Application
{
    protected $controller;
    protected $method;
    public $view;


    public function __construct($controller, $method)
    {
        parent::__construct();
        $this->controller = $controller;
        $this->method = $method;
        $this->view = new View();
    }


    protected function load_model($model)
    {
        if (class_exists($model)) {
            $this->{$model . 'Model'} = new $model(strtolower($model));
        }
    }

}