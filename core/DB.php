<?php

class DB
{
    // init DB
    private static $instance = null;
    private $pdo;
    private $query;
    private $sqlResult;
    private $error = false;
    private $count = 0;
    private $lastInsertId = null;

    private function __construct()
    {
        // connect to DB
        try {
            $this->pdo = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    // singleton
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new DB();
        }
        return self::$instance;
    }

    public function query($sql, $params = [], $class = false)
    {
        // reset error messages
        $this->error = false;
        if ($this->query = $this->pdo->prepare($sql)) {
            $tempCount = 1;
            // bind parameters for SQL queryv
            if (count($params)) {
                foreach ($params as $param) {
                    $this->query->bindValue($tempCount, $param);
                    $tempCount++;
                }
            }
            // if query is executed
            if ($this->query->execute()) {
                if ($class) {
                    $this->sqlResult = $this->query->fetchAll(PDO::FETCH_CLASS, $class);
                } else {
                    $this->sqlResult = $this->query->fetchAll(PDO::FETCH_OBJ);
                }
                $this->count = $this->query->rowCount();
                $this->lastInsertId = $this->pdo->lastInsertId();
            } else {
                $this->error = true;
            }
        }
        return $this;
    }

    protected function read($table, $params = [], $class)
    {
        $conditionString = '';
        $bind = [];
        $order = '';
        $limit = '';

        // conditions
        if (isset($params['conditions'])) {
            if (is_array($params['conditions'])) {
                foreach ($params['conditions'] as $condition) {
                    $conditionString .= ' ' . $condition . ' AND';
                }
                $conditionString = trim($conditionString);
                $conditionString = rtrim($conditionString, ' AND');
            } else {
                $conditionString = $params['conditions'];
            }
            if ($conditionString != '') {
                $conditionString = ' Where ' . $conditionString;
            }
        }

        // bind
        if (array_key_exists('bind', $params)) {
            $bind = $params['bind'];
        }

        // order
        if (array_key_exists('order', $params)) {
            $order = ' ORDER BY ' . $params['order'];
        }

        // limit
        if (array_key_exists('limit', $params)) {
            $limit = ' LIMIT ' . $params['limit'];
        }
        $sql = "SELECT * FROM {$table}{$conditionString}{$order}{$limit}";

        if ($this->query($sql, $bind, $class)) {
            if (!count($this->sqlResult)) return false;
            return true;
        }
        return false;
    }

    public function find($table, $params = [], $class = false)
    {
        if ($this->read($table, $params, $class)) {
            return $this->results();
        }
        return false;
    }

    public function findFirst($table, $params = [], $class = false)
    {
        if ($this->read($table, $params, $class)) {
            return $this->first();
        }
        return false;
    }

    public function insert($table, $fields = [])
    {
        $fieldString = '';
        $valueString = '';
        $values = [];

        foreach ($fields as $field => $value) {
            $fieldString .= '`' . $field . '`,';
            $valueString .= '?,';
            $values[] = $value;
        }

        $fieldString = rtrim($fieldString, ',');
        $valueString = rtrim($valueString, ',');
        $sql = "INSERT INTO {$table} ({$fieldString}) VALUES ({$valueString})";
        if (!$this->query($sql, $values)->error()) {
            return true;
        }
        return false;
    }

    public function update($table, $id, $fields = [])
    {
        $fieldString = '';
        $values = [];
        foreach ($fields as $field => $value) {
            $fieldString .= ' ' . $field . ' = ?,';
            $values[] = $value;
        }
        $fieldString = trim($fieldString);
        $fieldString = rtrim($fieldString, ',');
        $sql = "UPDATE {$table} SET {$fieldString} WHERE id = {$id}";
        if (!$this->query($sql, $values)->error()) {
            return true;
        }
        return false;
    }

    public function delete($table, $id)
    {
        $sql = "DELETE FROM {$table} WHERE id = {$id}";
        if (!$this->query($sql)->error()) {
            return true;
        }
        return false;
    }

    public function results()
    {
        return $this->sqlResult;
    }

    public function first()
    {
        return (!empty($this->sqlResult)) ? $this->sqlResult[0] : [];
    }

    public function getCount()
    {
        return $this->count;
    }

    public function lastID()
    {
        return $this->lastInsertId;
    }

    public function getColumns($table)
    {
        return $this->query("SHOW COLUMNS FROM {$table}")->results();
    }

    public function error()
    {
        return $this->error;
    }
}