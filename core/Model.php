<?php


class Model
{
    protected $db;
    protected $table;
    protected $modelName;
    protected $softDelete = true;

    public $id;

    public function __construct($table)
    {
        $this->db = DB::getInstance();
        $this->table = $table;
        $this->modelName = str_replace(' ', '', ucwords(str_replace('_', ' ', $this->table)));
    }

    public function find($params = [])
    {
        $params = $this->softDelete($params);
        $resultQuery = $this->db->find($this->table, $params, get_class($this));
        if (!$resultQuery) {
            return [];
        }
        return $resultQuery;
    }

    public function findFirst($params = [])
    {
        $params[] = $this->softDelete($params);
        $resultQuery = $this->db->findFirst($this->table, $params, get_class($this));
        return $resultQuery;
    }

    protected function populateObjectData($result)
    {
        foreach ($result as $key => $value) {
            $this->$key = $value;
        }
    }

    public function findById($id)
    {
        return $this->findFirst([
            'conditions' => "id=?",
            'bind' => [$id]
        ]);
    }

    public function insert($fields)
    {
        if (empty($fields)) {
            return false;
        }
        return $this->db->insert($this->table, $fields);
    }

    public function update($id, $fields)
    {
        if (empty($fields) || $id == '') {
            return false;
        }
        return $this->db->update($this->table, $id, $fields);
    }

    public function delete($id = '')
    {
        if ($id == '' && $this->id == '') {
            return false;
        }
        $id = ($id == '') ? $this->id : $id;
        if ($this->softDelete && $this->table != 'user_sessions') {
            // do not delete, but update only
            return $this->update($id, ['deleted' => 1]);
        }
        return $this->db->delete($this->table, $id);
    }

    public function query($sql, $bind = [])
    {
        return $this->db->query($sql, $bind);
    }

    public function data()
    {
        $data = new stdClass();
        foreach (getObjectProperties($this) as $column => $value) {
            $data->column = $value;
        }
        return $data;
    }

    public function save()
    {
        $fields = getObjectProperties($this);
        // update or insert
        if (property_exists($this, 'id') && $this->id != '') {
            return $this->update($this->id, $fields);
        }
        return $this->insert($fields);
    }

    public function assign($parameters)
    {
        if (!empty($parameters)) {
            foreach ($parameters as $key => $value) {
                if (property_exists($this, $key)) {
                    $this->$key = Input::sanitize($value);
                }
            }
            return true;
        }
        return false;
    }

    protected function softDelete($params)
    {
        if ($this->softDelete) {
            if (array_key_exists('conditions', $params)) {
                if (is_array($params['conditions'])) {
                    $params['conditions'][] .= " deleted !=1";
                } else {
                    $params['conditions'] .= " AND deleted != 1";
                }
            } else {
                $params['conditions'] = "deleted != 1";
            }
        }
        return $params;
    }
}