<?php


class View
{
    protected $head;
    protected $body;
    protected $siteTitle = DEFAULT_SITE_TITLE;
    protected $outputBuffer;
    protected $layout = DEFAULT_LAYOUT;

    public function __construct()
    {

    }

    public function render($viewName)
    {
        // mac, win compatibility
        $viewArray = explode('/', $viewName);
        $viewString = implode(DS, $viewArray);

        if (file_exists(ROOT . DS . 'app' . DS . 'views' . DS . $viewString . '.php')) {
            include_once(ROOT . DS . 'app' . DS . 'views' . DS . $viewString . '.php');
            include_once(ROOT . DS . 'app' . DS . 'views' . DS . 'layouts' . DS . $this->layout . '.php');
        } else {
            die("The view not defined");
        }
    }

    public function content($type)
    {
        if ($type == 'head') {
            return $this->head;
        } elseif ($type == 'body') {
            return $this->body;
        }
        return false;

    }

    public function start($type)
    {
        // set to head or body
        $this->outputBuffer = $type;
        // output buffer
        ob_start();
    }

    public function end()
    {
        if ($this->outputBuffer == 'head') {
            $this->head = ob_get_clean();
        } elseif ($this->outputBuffer == 'body') {
            $this->body = ob_get_clean();
        } else {
            die('Output buffer not defined');
        }
    }

    public function siteTitle()
    {
        return $this->siteTitle;
    }

    public function setTitle($title)
    {
        $this->siteTitle = $title;
    }

    public
    function setLayout($path)
    {
        $this->layout = $path;
    }

    public function insert($path)
    {
        include_once ROOT . DS . 'app' . DS . 'views' . DS . $path . '.php';
    }

    public function partial($group, $partial)
    {
        include_once ROOT . DS . 'app' . DS . 'views' . DS . $group . DS . 'partials' . DS . $partial . '.php';
    }
}