<?php


class Validate
{
    private $passed = false;
    private $errors = [];
    private $db = null;

    public function __construct()
    {
        $this->db = DB::getInstance();
    }

    /**
     * @param $source $_POST data
     * @param array $items form validation
     */
    public function check($source, $items = [], $csrfCheck = false)
    {
        $this->errors = [];
        if ($csrfCheck) {
            $csrfPass = FormHelpers::checkToken($source['csrf_token']);
            if (!isset($source['csrf_token']) || !$csrfPass) {
                $this->addError(['Something has gone wrong', 'csrf_token']);
            }
        }
        foreach ($items as $item => $rules) {
            $item = Input::sanitize($item);
            $display = $rules['display'];
            foreach ($rules as $rule => $rule_value) {
                $value = Input::sanitize(trim($source[$item]));

                // rules
                if ($rule === 'required' && empty($value)) {
                    $this->addError([
                        "{$display} is required",
                        $item
                    ]);
                } elseif (!empty($value)) {
                    switch ($rule) {
                        case 'min':
                            if (strlen($value) < $rule_value) {
                                $this->addError(["{$display} must be a minimum of {$rule_value} characters", $item]);
                            }
                            break;
                        case 'max':
                            if (strlen($value) > $rule_value) {
                                $this->addError(["{$display} must be a maximum of {$rule_value} characters", $item]);
                            }
                            break;
                        case 'matches':
                            if ($value != $source[$rule_value]) {
                                $matchDisplay = $items[$rule_value]['display'];
                                $this->addError(["{$matchDisplay} and {$display} must match", $item]);
                            }
                            break;
                        case 'unique':
                            $check = $this->db->query("SELECT {$item} FROM {$rule_value} WHERE {$item} = ?", [$value]);
                            if ($check->getCount()) {
                                $this->addError(["{$display} already exists. Choose another {$display}", $item]);
                            }
                            break;
                        case 'unique_update':
                            $t = explode(',', $rule_value);
                            $table = $t[0];
                            $id = $t[1];
                            $query = $this->db->query("SELECT * FROM {$table} WHERE id != ? AND {$item} = ?", [$id, $value]);
                            if ($query->getCount()) {
                                $this->addError(["{$display} already exists. Please choose another {$display}", $item]);
                            }
                            break;
                        case 'is_numeric':
                            if (!is_numeric($value)) {
                                $this->addError(["{$display} has to be a number. Use numeric value", $item]);
                            }
                            break;
                        case 'valid_email':
                            if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                                $this->addError(["{$display} must be a valid email address"]);
                            }
                            break;
                    }
                }
            }
        }

        if (empty($this->errors)) {
            $this->passed = true;
        }
        return $this;
    }

    public function addError($error)
    {
        $this->errors[] = $error;
        if (empty($this->errors)) {
            $this->passed = true;
        } else {
            $this->passed = false;
        }
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return bool
     */
    public function passed()
    {
        return $this->passed;
    }

    public function displayErrors()
    {
        $html = '<ul class="bg-danger">';
        foreach ($this->errors as $error) {
            if (is_array($error)) {
                $html .= '<li class=text-danger">' . $error[0] . '</li>';
            } else {
                $html .= '<li class=text-danger">' . $error . '</li>';
            }
        }
        $html .= '</ul>';

        return $html;

    }
}