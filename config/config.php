<?php

define('DEBUG', true);

//DB
define('DB_NAME', 'customMVC');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_HOST', '127.0.0.1');


// default controller and action if nothing is passed in the URL
define('DEFAULT_CONTROLLER', 'HomeController');
define('DEFAULT_ACTION', 'indexAction');
// default layout if not specified
define('DEFAULT_LAYOUT', 'default');
define('DEFAULT_SITE_TITLE', 'MVC framework');

define('PROJECT_ROOT', '/customMVC/');

//session name for logged in user
define('CURRENT_USER_SESSION_NAME', 'kwXfewCfasdfaDSA');
// cookie name for logged in user remember me
define('REMEMBER_ME_COOKIE_NAME', 'JAMFSADfdsf32f');
// cookie expire in seconds (cca 30 days)
define('REMEMBER_ME_COOKIE_EXPIRY', 640000);

define('ACCESS_RESTRICTED', 'RestrictedController');

