<?php $this->setTitle('Login'); ?>

<?php $this->start('head'); ?>
<meta content="test"/>
<?php $this->end(); ?>

<?php $this->start('body'); ?>
<div class="col-4 offset-4 jumbotron">
    <form action="<?= PROJECT_ROOT ?>register/login" class="form" method="post">
        <?= FormHelpers::csrfInput() ?>
        <div class="bg-danger">
            <?= $this->displayErrors ?>
        </div>
        <h3 class="text-center">Login</h3>
        <div class="form-group">
            <label for="username">Username</label>
            <input type="text" name="username" id="username" value="" class="form-control"/>
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" name="password" id="password" value="" class="form-control"/>
        </div>
        <div class="form-group">
            <label for="remember_me">
                <input type="checkbox" id="remember_me" name="remember_me">
                Remember me
            </label>
        </div>
        <div class="form-group">
            <input type="submit" value="Login" class="btn btn-primary">
        </div>
        <div class="text-right">
            <a href="<?= PROJECT_ROOT ?>register/register" class="text-primary">Register</a>
        </div>
    </form>
</div>
<?php $this->end(); ?>



