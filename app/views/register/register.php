<?php $this->setTitle('RegisterController'); ?>

<?php $this->start('head'); ?>
<meta content="test"/>
<?php $this->end(); ?>

<?php $this->start('body'); ?>
<div class="col-4 offset-4 jumbotron">
    <form action="" class="form" method="post">
        <?= FormHelpers::csrfInput() ?>
        <div class="bg-danger">
            <?= $this->displayErrors ?>
        </div>
        <h3 class="text-center">Register account</h3>
        <div class="form-group">
            <label for="first_name">First name</label>
            <input type="text" name="first_name" id="first_name" value="<?= $this->post['first_name'] ?>" class="form-control"/>
        </div>
        <div class="form-group">
            <label for="last_name">Last name</label>
            <input type="text" name="last_name" id="last_name" value="<?= $this->post['last_name'] ?>" class="form-control"/>
        </div>
        <div class="form-group">
            <label for="username">Username</label>
            <input type="text" name="username" id="username" value="<?= $this->post['username'] ?>" class="form-control"/>
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" name="email" id="email" value="<?= $this->post['email'] ?>" class="form-control"/>
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" name="password" id="password" value="<?= $this->post['password'] ?>" class="form-control"/>
        </div>
        <div class="form-group">
            <label for="confirm">Confirm password</label>
            <input type="password" name="confirm" id="confirm" value="<?= $this->post['confirm'] ?>" class="form-control"/>
        </div>
        <div class="form-group">
            <label for="remember_me">
                <input type="checkbox" id="remember_me" name="remember_me">
                Remember me
            </label>
        </div>
        <div class="float-right">
            <input type="submit" class="btn btn-primary btn-lg" value="Register" />
        </div>
    </form>
</div>
<?php $this->end(); ?>



