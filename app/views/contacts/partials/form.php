<form class="form" accept-charset="<?= $this->postAction ?>" method="post">
    <div class="bg-danger form-errors">
        <?= $this->displayErrors ?>
    </div>

    <?= inputBlock('text', 'First Name', 'first_name', $this->contact->first_name, ['class' => 'form-control'], ['class' => 'forum-group col-md-5']); ?>
    <?= inputBlock('text', 'Last Name', 'last_name', $this->contact->last_name, ['class' => 'form-control'], ['class' => 'forum-group col-md-5']); ?>
    <?= inputBlock('text', 'Address', 'address', $this->contact->address, ['class' => 'form-control'], ['class' => 'forum-group col-md-5']); ?>
    <?= inputBlock('text', 'Address 2', 'address2', $this->contact->first_name, ['class' => 'form-control'], ['class' => 'forum-group col-md-5']); ?>
    <?= inputBlock('text', 'Eamil', 'email', $this->contact->email, ['class' => 'form-control'], ['class' => 'forum-group col-md-5']); ?>
    <?= inputBlock('text', 'Home Phone', 'home_phone', $this->contact->home_phone, ['class' => 'form-control'], ['class' => 'forum-group col-md-5']); ?>
    <?= inputBlock('text', 'Cell Phone', 'cell_phone', $this->contact->cell_phone, ['class' => 'form-control'], ['class' => 'forum-group col-md-5']); ?>
    <?= inputBlock('text', 'Work Phone', 'work_phone', $this->contact->work_phone, ['class' => 'form-control'], ['class' => 'forum-group col-md-5']); ?>
    <?= inputBlock('text', 'City', 'city', $this->contact->city, ['class' => 'form-control'], ['class' => 'forum-group col-md-5']); ?>
    <?= inputBlock('text', 'State', 'state', $this->contact->state, ['class' => 'form-control'], ['class' => 'forum-group col-md-5']); ?>
    <?= inputBlock('text', 'zip', 'zip', $this->contact->zip, ['class' => 'form-control'], ['class' => 'forum-group col-md-5']); ?>
    <?= inputBlock('text', 'Country', 'country', $this->contact->country, ['class' => 'form-control'], ['class' => 'forum-group col-md-5']); ?>

    <div class="text-right">
        <a href="<?=PROJECT_ROOT.DS?>contacts" class="btn btn-default">Cancel</a>
            <?=submitTag('Save',['class'=>'btn btn-primary']);?>

    </div>
</form>