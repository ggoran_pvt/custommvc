<?php $this->setTitle('Edit contact'); ?>

<?php $this->start('body'); ?>
<div class="container col-md-8 col-md-offset-2 well">
    <h2 class="text-center">
        Edit contact: <?=$this->contact->displayName()?>
    </h2>
    <hr>
    <?php $this->partial('contacts','form') ?>
</div>

<?php $this->end(); ?>



