<?php $this->setTitle('Add a contact'); ?>

<?php $this->start('body'); ?>
<div class="container col-md-8 col-md-offset-2 well">
    <h2 class="text-center">
        Add a contact
    </h2>
    <hr>
    <?php $this->partial('contacts','form') ?>
</div>

<?php $this->end(); ?>



