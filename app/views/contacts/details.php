<?php $this->setTitle($this->contact->first_name); ?>

<?php $this->start('body'); ?>
<div class="container">
    <div class="col-md-8">
        <a href="<?= PROJECT_ROOT ?>contacts" class="btn btn-default">Back</a>
        <h2 class="text-center"><?= $this->contact->displayName() ?></h2>
        <div class="col-md-6">
            <p><span class="font-weight-bold">Email: </span><?= $this->contact->email ?></p>
            <p><span class="font-weight-bold">Cell Phone: </span><?= $this->contact->cell_phone ?></p>
            <p><span class="font-weight-bold">Work Phone: </span><?= $this->contact->work_phone ?></p>
            <p><span class="font-weight-bold"> Address: </span><?= $this->contact->address ?></p>
            <p><span class="font-weight-bold"> Country: </span><?= $this->contact->country ?></p>
        </div>
        <div class="col-md-6">
            <p><span class="font-weight-bold"> Address: </span>
                <?= $this->contact->displayAddressLabel() ?>
            </p>
        </div>
    </div>
</div>

<?php $this->end(); ?>



