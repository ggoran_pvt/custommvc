<?php
$menu = Router::getMenu('menu_acl');

?>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_menu"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="main_menu">
        <ul class="navbar-nav mr-auto">
            <?php
            foreach ($menu as $key => $value):
                $active = '';
                $currentPage = currentPage();

                if (is_array($value)):
                    ?>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                           data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            <?= $key ?>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <?php foreach ($value as $k => $v):
                                $active = ($v == $currentPage) ? 'active' : '';
                                if ($k == 'separator'): ?>
                                    <div class="dropdown-divider"></div>
                                <?php else: ?>
                                    <a class="dropdown-item <?= $active ?>" href="<?= $v ?>"><?= $k ?></a>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                    </li>

                <?php
                else :
                    $active = ($value == $currentPage) ? 'active' : '';
                    ?>
                <li class="nav-item">
                    <a class="dropdown-item <?= $active ?>" href="<?= $value ?>"><?= $key ?></a>
                </li>
                <?php
                endif;
            endforeach;
            ?>
        </ul>

        <ul class="nav justify-content-end">
            <?php if (currentUser()): ?>
                <li class="nav-item">
                    <a class="nav-link active" href="#">Hello <?= get_current_user(); ?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="<?=PROJECT_ROOT?>/register/logout">Logout</a>
                </li>
            <?php else: ?>
                <li class="nav-item">
                    <a class="nav-link active" href="<?=PROJECT_ROOT?>/register/register">Register</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="<?=PROJECT_ROOT?>/register/login">Login</a>
                </li>
            <?php endif; ?>
        </ul>
    </div>
</nav>