<?php

class Contacts extends Model
{

    public $id, $first_name, $last_name, $email, $address, $address2, $city, $state, $zip, $user_id;
    public $home_phone, $cell_phone, $work_phone, $deleted = 0;

    public function __construct()
    {
        $table = 'contacts';
        parent::__construct($table);
        $this->softDelete = true;
    }

    public static $addValidation = [
        'first_name' => [
            'display' => 'First Name',
            'required' => true,
            'max' => 155
        ],
        'last_name' => [
            'display' => 'Last Name',
            'required' => true,
            'max' => 155
        ]
    ];

    public function findAllByUserId($user_id, $params = [])
    {
        $conditions = [
            'conditions' => 'user_id = ?',
            'bind' => [$user_id]
        ];
        $conditions = array_merge($conditions, $params);
        return $this->find($conditions);
    }

    public function displayName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }


    public function findByIdAndUserId($contactId, $userId, $params = [])
    {
        $conditions = [
            'conditions' => 'id = ? AND user_id = ?',
            'bind' => [$contactId, $userId]
        ];
        $conditions = array_merge($conditions, $params);
        return $this->findFirst($conditions);
    }

    public function displayAddress()
    {
        $address = '<br>';

        if (!empty($this->address)) {
            $address .= $this->address . '<br>';
        }
        if (!empty($this->address2)) {
            $address .= $this->address2 . '<br>';
        }
        if (!empty($this->city)) {
            $address .= $this->city . '<br>';
        }
        if (!empty($this->country)) {
            $address .= $this->country . '<br>';
        }
        if (!empty($this->zip)) {
            $address .= $this->zip . '<br>';
        }
        return $address;
    }

    public function displayAddressLabel()
    {
        $html = $this->displayAddress() . '<br>';
        return $html;
    }
}