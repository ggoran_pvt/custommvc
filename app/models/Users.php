<?php


class Users extends Model
{
    private $isLoggedIn;
    private $sessionName;
    private $cookieName;
    public static $currentLoggedInUser = null;
    public $id, $username, $email, $password, $first_name, $last_name, $acl, $deleted = 0;

    public function __construct($user = '')
    {
        $table = 'users';
        parent::__construct($table);
        //
        $this->sessionName = CURRENT_USER_SESSION_NAME;
        $this->cookieName = REMEMBER_ME_COOKIE_NAME;
        $this->softDelete = true;
        if ($user != '') {
            if (is_int($user)) {
                $u = $this->db->findFirst('users', ['conditions' => 'id = ?', 'bind' => [$user]], 'Users');
            } else {
                $u = $this->db->findFirst('users', ['conditions' => 'username = ?', 'bind' => [$user]], 'Users');
            }
            if ($u) {
                foreach ($u as $key => $value) {
                    $this->$key = $value;
                }
            }
        }
    }

    public static function currentLoggedInUser()
    {
        if (!isset(self::$currentLoggedInUser) && Session::exists(CURRENT_USER_SESSION_NAME)) {
            $user = new Users((int)Session::get(CURRENT_USER_SESSION_NAME));
            self::$currentLoggedInUser = $user;
        }
        return self::$currentLoggedInUser;
    }

    public static function loginUserFromCookie()
    {
        $userSession = UserSessions::getFromCookie();
        if ($userSession->user_id != '') {
            $user = new self((int)$userSession->user_id);
        }
        if ($user) {
            $user->login();
        }
        return $user;
    }

    public function findByUsername($username)
    {
        return $this->findFirst(['conditions' => "username = ?", 'bind' => [$username]]);
    }

    public function login($rememberMe = false)
    {
        Session::set($this->sessionName, $this->id);
        if ($rememberMe) {
            $hash = md5(uniqid() + rand(0, 100));
            $user_agent = Session::useragent_no_version();
            Cookie::set($this->cookieName, $hash, REMEMBER_ME_COOKIE_EXPIRY);
            $fields = [
                'session' => $hash,
                'user_agent' => $user_agent,
                'user_id' => $this->id,
            ];
            // delete old entry
            $this->db->query("DELETE FROM user_sessions WHERE user_id = ? AND user_agent = ?", [$this->id, $user_agent]);
            $this->db->insert('user_sessions', $fields);
        }
    }

    public function logout()
    {
        $userAgent = Session::useragent_no_version();
        $userSession = UserSessions::getFromCookie();
        $userSession->delete();

        Session::delete(CURRENT_USER_SESSION_NAME);
        if (Cookie::exists(REMEMBER_ME_COOKIE_NAME)) {
            Cookie::delete(REMEMBER_ME_COOKIE_NAME);
        }
        self::$currentLoggedInUser = null;
        return true;
    }

    public function registerNewUser($params)
    {
        $this->assign($params);
        $this->password = password_hash($this->password, PASSWORD_DEFAULT);
        $this->deleted = 0;
        $this->acl = 'USER';
        $this->save();
    }


    function acls()
    {
        if (empty($this->acl)) {
            return [];
        }
        return json_decode($this->acl, true);
    }

}