<?php

function dnd($data)
{
    echo '<pre>';
    var_dump($data);
    echo '</pre>';
    die();
}

function sanitize($value)
{
    return htmlentities($value, ENT_QUOTES, 'UTF-8');
}


function currentUser()
{
    return Users::currentLoggedInUser();
}

function posted_values($post)
{
    $clean_array = [];
    foreach ($post as $key => $value) {
        $clean_array[$key] = Input::sanitize($value);
    }
    return $clean_array;
}

function currentPage()
{
    $currentPage = $_SERVER['REQUEST_URI'];
    if ($currentPage == PROJECT_ROOT || $currentPage == PROJECT_ROOT . 'home/index') {
        $currentPage = PROJECT_ROOT . 'home';
    }
    return $currentPage;
}

function getObjectProperties($object)
{
    return get_object_vars($object);
}