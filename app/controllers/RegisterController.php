<?php


class RegisterController extends Controller
{
    public function __construct($controller, $method)
    {
        parent::__construct($controller, $method);
        $this->load_model('Users');
        $this->view->setLayout('default');
    }

    public function loginAction()
    {
        $validation = new Validate();
        if ($_POST) {
            // form validation
            $validation->check($_POST, [
                'username' => [
                    'display' => 'Username',
                    'required' => true,
                ],
                'password' => [
                    'display' => 'Password',
                    'required' => true,
                    'min' => 2,
                ],
            ], true);
            if ($validation->passed()) {
                $user = $this->UsersModel->findByUsername($_POST['username']);
                if ($user && password_verify(Input::get('password'), $user->password)) {
                    $remember = (isset($_POST['remember_me']) && Input::get('remember_me')) ? true : false;
                    $user->login($remember);
                    Router::redirect('');
                }
            } else {
                $validation->addError("Error with user or pass.");
            }
        }
        $this->view->displayErrors = $validation->displayErrors();
        $this->view->render('register/login');
    }


    public function logoutAction()
    {
        if (currentUser()) {
            currentUser()->logout();
        }
        Router::redirect('');
    }

    public function registerAction()
    {
        $validation = new Validate();
        $posted_values = [
            'first_name' => '',
            'last_name' => '',
            'username' => '',
            'email' => '',
            'password' => '',
            'confirm ' => ''
        ];

        if ($_POST) {
            $posted_values = posted_values($_POST);
            $validation->check($_POST, [
                'first_name' => [
                    'display' => 'First Name',
                    'required' => true,
                ],
                'last_name' => [
                    'display' => 'Last Name',
                    'required' => true,
                ],
                'username' => [
                    'display' => 'Username',
                    'unique' => 'users',
                    'max' => 150,
                    'required' => true,
                ],
                'email' => [
                    'display' => 'Email',
                    'max' => 150,
                    'unique' => 'users',
                    'required' => true,
                    'valid_email' => true,
                ],
                'password' => [
                    'display' => 'Password',
                    'min' => 4,
                    'required' => true,
                ],
                'confirm' => [
                    'display' => 'Repeat password',
                    'matches' => 'password',
                    'required' => true,
                ]
            ], true);

            if ($validation->passed()) {
                $newUser = new Users();
                $newUser->registerNewUser($_POST);
                $newUser->login();
                Router::redirect('');
            }
        }

        $this->view->post = $posted_values;
        $this->view->displayErrors = $validation->displayErrors();

        $this->view->render('register/register');
    }

}