<?php


class HomeController extends Controller
{

    public function __construct($controller, $method)
    {
        parent::__construct($controller, $method);
    }

    public function indexAction($name = [])
    {
        $this->view->render('home/index');
    }
}